<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>HomeWork8</title>
	<style type="text/css">
		body {
			background-color: silver; 
		}
	</style>
</head>
<body>
	<form action="./Action" method="get">
		<table border="1">
			<tr>
				<th width="100"><c:out value="Name"/></th>
				<th width="100"><c:out value="Value"/></th>
				<th width="50"><c:out value="Action"/></th>
			</tr>
			<c:forEach var="e" items="${sessionScope}">
				<tr>
					<td><c:out value="${e.key}"/></td>
					<c:forEach var="v" items="${e.value}" end="0">
						<td><c:out value="${v}"/></td>
						<td><a href="./Action?action=remove&name=${e.key}&value=${v}">delete</a></td>
					</c:forEach>
				</tr>
				<c:forEach var="v" items="${e.value}" begin="1">
					<tr>
						<td>&nbsp;</td>
						<td><c:out value="${v}"/></td>
						<td><a href="./Action?action=remove&name=${e.key}&value=${v}">delete</a></td>
					</tr>
				</c:forEach>
			</c:forEach>
			<tr>
				<td><input type="text" name="name"></td>
				<td><input type="text" name="value"></td>
				<td><input type="submit" name="action" value="add"></td>
			</tr>
		</table>
	</form>
</body>
</html>