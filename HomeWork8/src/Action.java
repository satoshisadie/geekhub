

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Action
 */
public class Action extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Action() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    @SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		String name = request.getParameter("name");
		String value = request.getParameter("value");
		HttpSession session = request.getSession();
		
		Set<String> set = (Set<String>) session.getAttribute(name);
		if ("add".equalsIgnoreCase(action)) {	
			if (set != null) {
				if (!set.contains(value)) {
					set.add(value);
				}
			} else {
				set = new HashSet<String>();
				set.add(value);
				session.setAttribute(name, set);
			}
		} else if ("remove".equalsIgnoreCase(action)) {
			if (set.size() == 1) {
				session.removeAttribute(name);
			} else {
				set.remove(value);
			}
		}
		
		request.getRequestDispatcher("/index.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
