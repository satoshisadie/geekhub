import java.util.*;

public class Cat {
	private int[] rgbColor;
	private int age;
	private String name;
	private List<Cat> kittens;

	Cat() {
		this.rgbColor = new int[3];
		this.age = 1;
		this.kittens = new ArrayList<>();
	}

	Cat(String name, int age, int red, int green, int blue) {
		this.rgbColor = new int[3];
		rgbColor[0] = red;
		rgbColor[1] = green;
		rgbColor[2] = blue;
		this.age = age;
		this.name = name;
		this.kittens = new ArrayList<>();
	}

	public void birthOfAKitten(Cat kitten) {
		this.kittens.add(kitten);
	}

	public String toString() {
		return "Name: " + this.name + " Color: red(" + this.rgbColor[0] + ") " + "green(" + this.rgbColor[1] + ") " + "blue(" + this.rgbColor[2] + ")" + " Age: " + this.age;
	}

	public int hashCode() {
		int result = 31;
		result = 31 * result + new Integer(rgbColor[0]).hashCode();
		result = 31 * result + new Integer(rgbColor[1]).hashCode();
		result = 31 * result + new Integer(rgbColor[2]).hashCode();
		result = 31 * result + new Integer(age).hashCode();
		return result;
	}

	@Override
	public boolean equals(Object cat) {
		if (this == cat)
			return true;
		if (cat instanceof Cat) {
			Cat newCat = (Cat) cat;
			if (this.rgbColor[0] == newCat.rgbColor[0] && this.rgbColor[1] == newCat.rgbColor[1] && this.rgbColor[2] == newCat.rgbColor[2] && this.age == newCat.age) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
}
