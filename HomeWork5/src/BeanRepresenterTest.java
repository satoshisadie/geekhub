public class BeanRepresenterTest {
	public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException {
		Cat Murka = new Cat("Murka", 5, 130, 20, 10);
		Murka.birthOfAKitten(new Cat("Tom", 0, 130, 20, 10));
		Murka.birthOfAKitten(new Cat("Murzik", 0, 20, 40, 7));
		Murka.birthOfAKitten(new Cat("Barsik", 0, 10, 150, 40));
		BeanRepresenter representer = new BeanRepresenter();
		representer.represent(Murka, "");
	}
}
