import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.*;

public class BeanRepresenter {

	private List<Object> isVisited;

	public BeanRepresenter() {
		isVisited = new ArrayList<Object>();
	}

	private void representArray(Object array, String shift) {
		System.out.print(shift + "{ ");
		for (int i = 0; i < Array.getLength(array); i++) {
			if (Array.get(array, i) == null) {
				System.out.print(Array.get(array, i));
			} else {
				System.out.print(Array.get(array, i).toString());
			}
			if (i != Array.getLength(array) - 1) {
				System.out.print(", ");
			}
		}
		System.out.println(" }");
	}

	public void represent(Object obj, String shift) throws IllegalArgumentException, IllegalAccessException {
		Class clazz = obj.getClass();
		System.out.println(shift + "Class " + clazz.getName());
		Field[] objectFields = clazz.getDeclaredFields();
		for (Field field : objectFields) {
			field.setAccessible(true);
			Class fieldType = field.getType();
			Object lObj = field.get(obj); //Local object
			if (lObj == null) {
				System.out.println(shift + field.getName() + ": null");
			} else if (fieldType.isPrimitive() || lObj instanceof String || lObj instanceof Number || lObj instanceof Character) {
				System.out.println(shift + field.getName() + ": " + lObj);
			} else if (fieldType.isArray()) {
				System.out.println(shift + field.getName() + ": ");
				representArray(lObj, shift);
			} else if (!fieldType.isPrimitive() && !isVisited.contains(lObj)) {
				isVisited.add(lObj);
				represent(lObj, shift + "|  ");
			}
		}
		System.out.println(shift + "end");
	}
}
