package jdbc;

import java.sql.*;
import java.util.*;

public class Task4 {
	static Scanner in = new Scanner(System.in);

	static Connection connect() throws SQLException {
		String url = "jdbc:mysql://";
		System.out.print("Enter host: ");
		String host = in.nextLine();
		System.out.print("Enter login: ");
		String login = in.nextLine();
		System.out.print("Enter password: ");
		String password = in.nextLine();
		System.out.print("Enter database: ");
		String database = in.nextLine();
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(url + host + "/"
					+ database, login, password);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return connection;
	}

	static void query(Connection connection) throws SQLException {
		System.out.print("Enter query: ");
		String query = in.nextLine();
		Statement statement = connection.createStatement();
		if (queryType(query) == 1) {
			ResultSet result;
			try {
				result = statement.executeQuery(query);
			} catch (SQLException e) {
				System.out.println(e.getMessage());
				return;
			}
			ResultSetMetaData RSmeta = result.getMetaData();
			int rowsN = RSmeta.getColumnCount();
			int[] maxSizes = new int[rowsN];
			for (int i = 0; i < rowsN; i++) {
				maxSizes[i] = RSmeta.getColumnName(i + 1).length();
			}
			result.last();
			while (result.previous()) {
				for (int i = 0; i < maxSizes.length; i++) {
					maxSizes[i] = Math.max(maxSizes[i], result.getString(i + 1)
							.length());
				}
			}
			for (int i = 1; i <= rowsN; i++) {
				String columnName = RSmeta.getColumnName(i);
				System.out.print(columnName);
				int additionalSize = 0;
				while (columnName.length() + additionalSize < maxSizes[i - 1]) {
					System.out.print(" ");
					additionalSize++;
				}
				System.out.print("|");
			}
			System.out.println();
			while (result.next()) {
				for (int i = 1; i <= rowsN; i++) {
					String columnData = result.getString(i);
					System.out.print(columnData);
					int additionalSize = 0;
					while (columnData.length() + additionalSize < maxSizes[i - 1]) {
						System.out.print(" ");
						additionalSize++;
					}
					System.out.print("|");
				}
				System.out.println();
			}
		} else {
			int changed;
			try {
				changed = statement.executeUpdate(query);
			} catch (SQLException e) {
				System.out.println(e.getMessage());
				return;
			}
			System.out.println("Lines changed: " + changed);
		}
	}

	static int queryType(String query) {
		StringTokenizer check = new StringTokenizer(query);
		while (check.hasMoreTokens()) {
			String token = check.nextToken();
			if ("SELECT".equalsIgnoreCase(token)) {
				return 1;
			}
		}
		return 0;
	}

	static public void main(String[] args) throws ClassNotFoundException,
			SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection connection = connect();
		if (connection == null)
			return;
		String command;
		do {
			query(connection);
			System.out.println("Do you want to do another query? (yes/no)");
			command = in.nextLine();
		} while (command.equalsIgnoreCase("yes"));
		connection.close();
	}

}
