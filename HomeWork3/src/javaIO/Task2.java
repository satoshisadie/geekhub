package javaIO;

import java.io.*;
import java.util.*;

public class Task2 {
	static Scanner in = new Scanner(System.in);

	static public void main(String[] args) throws IOException {
		System.out.println("Select encoding type");
		System.out.println("1: UTF8");
		System.out.println("2: ISO8859_4");
		System.out.println("3: ISO8859_6");
		System.out.println("4: Big5");
		System.out.println("5: ISO8859_2");
		int type = in.nextInt();
		String eType;
		switch (type) {
		case 1:
			eType = "UTF8";
			break;
		case 2:
			eType = "ISO8859_4";
			break;
		case 3:
			eType = "ISO8859_6";
			break;
		case 4:
			eType = "Big5";
			break;
		case 5:
			eType = "ISO8859_2";
			break;
		default:
			eType = "UTF8";
			break;
		}
		String file = "c:\\workspace\\textFile.txt";
		Reader reader;
		Writer writer;
		try {
			reader = new InputStreamReader(new FileInputStream(file), "Big5");
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return;
		}
		List<Integer> list = new ArrayList<>();
		int cr;
		while ((cr = reader.read()) >= 0) {
			list.add(cr);
		}
		reader.close();
		try {
			writer = new OutputStreamWriter(new FileOutputStream(file), eType);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return;
		}
		for (int i = 0; i < list.size(); i++) {
			writer.write(list.get(i));
		}
		writer.close();
	}
}
