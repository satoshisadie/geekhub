package javaIO;

import java.io.*;
import java.util.*;

public class Task3 {
	static Scanner in = new Scanner(System.in);

	static public void main(String[] args) {
		System.out.print("Enter path: ");
		File to = new File(in.nextLine());
		System.out.println("Choose how to copy:");
		System.out.println("1: With buffering");
		System.out.println("2: Without Buffering");
		int how = in.nextInt();
		File from = new File("c:\\workspace\\toCopy.avi");
		try {
			if (!to.createNewFile()) {
				System.out.println("File already exist");
				return;
			}
		} catch (IOException e) {
			System.out.println(e.getMessage());
			return;
		}

		if (how == 1) {
			BufferedInputStream input = null;
			BufferedOutputStream output = null;
			try {
				input = new BufferedInputStream(new FileInputStream(from));
				output = new BufferedOutputStream(new FileOutputStream(to));
				long start = System.currentTimeMillis();
				int c;
				while ((c = input.read()) >= 0) {
					output.write(c);
				}
				output.flush();
				long end = System.currentTimeMillis();
				System.out.println("Time:" + (end - start) + "ms");
			} catch (IOException e) {
				System.out.println(e.getMessage());
				return;
			} finally {
				try {
					input.close();
					output.close();
				} catch (IOException e) {
					System.out.println(e.getMessage());
				}
			}
		} else {
			FileInputStream input = null;
			FileOutputStream output = null;
			try {
				input = new FileInputStream(from);
				output = new FileOutputStream(to);
				byte[] data = new byte[8000];
				int size;
				long start = System.currentTimeMillis();
				while ((size = input.read(data)) >= 0) {
					output.write(data, 0, size);
				}
				long end = System.currentTimeMillis();
				System.out.println("Time:" + (end - start) + "ms");
			} catch (IOException e) {
				System.out.println(e.getMessage());
				return;
			} finally {
				try {
					input.close();
					output.close();
				} catch (IOException e) {
					System.out.println(e.getMessage());
				}
			}
		}
	}
}
