package javaIO;

import java.io.File;

public class Task1 {
	public static void main(String[] args) {
		String dirName;
		int operation;
		try {
			dirName = args[0];
			operation = Integer.parseInt(args[1]);
		} catch (Exception e) {
			System.out.println("You must enter 2 parameters");
			return;
		}
		File newDir = new File("c:\\", dirName);
		switch (operation) {
		case 1:
			newDir.mkdir();
			break;
		case 2:
			newDir.delete();
			break;
		case 3:
			newDir.renameTo(new File("c:\\renamedDir"));
			break;
		default:
			break;
		}
	}
}
