package lang;

public class MyExceptionsTest {
	static void print1(int toPrint) { // RuntimeException is unchecked
		if (toPrint == 3) {
			throw new MyException1(toPrint);
		}
		System.out.println("print1: " + toPrint);
	}

	static void print2(int toPrint) throws MyException2 { // Exception is checked
		if (toPrint == 4) {
			throw new MyException2(toPrint);
		}
		System.out.println("print2: " + toPrint);
	}

	public static void main(String[] args) {
		for (int i = 0; i < 5; i++) {
			try {
				print1(i);
				print2(i);
			} catch (MyException1 e) {
				System.out.println("Catched " + e);
			} catch (MyException2 e) {
				System.out.println("Catched " + e);
			}
		}
	}
}
