package lang;

public class CatTest {
	public static void main(String[] args) {
		Cat murka = new Cat(2, 1, 100, 4);
		Cat tom = new Cat(2, 1, 100, 4);
		Cat murzik = new Cat(2, 1, 99, 4);
		System.out.println(murka.equals(tom));
		System.out.println(murka.equals(murzik));
		System.out.println(murka.equals("other class"));
		System.out.println(murka.toString());
		System.out.println(murzik.toString());
	}
}
