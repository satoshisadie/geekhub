package lang;

public class MyException2 extends Exception {
	private int information;

	public MyException2(int information) {
		this.information = information;
	}

	public String toString() {
		return "MyException2: " + information;
	}
}