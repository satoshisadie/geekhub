package lang;

public class Register {
	static String switchRegister(String toSwitch) {
		if (Character.isLowerCase(toSwitch.charAt(0))) {
			toSwitch = Character.toUpperCase(toSwitch.charAt(0)) + toSwitch.substring(1);
		} else {
			toSwitch = Character.toLowerCase(toSwitch.charAt(0)) + toSwitch.substring(1);
		}
		return toSwitch;
	}

	public static void main(String[] args) {
		System.out.println(switchRegister("sTRING"));
		System.out.println(switchRegister("String"));
	}
}
