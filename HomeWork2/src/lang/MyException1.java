package lang;

public class MyException1 extends RuntimeException {
	private int information;

	public MyException1(int information) {
		this.information = information;
	}

	public String toString() {
		return "MyException1: " + information;
	}
}
