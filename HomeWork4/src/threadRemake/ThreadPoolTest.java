package threadRemake;

import java.io.*;
import java.util.*;

class FileFinder implements Runnable {
	private String filePattern; // Pattern to search
	private File directory; // Start directory
	private ThreadPool pool; // Pool of threads

	public FileFinder(String filePattern, File directory, ThreadPool pool) {
		this.pool = pool;
		this.filePattern = filePattern;
		this.directory = directory;
	}

	public void run() {
		File[] files = directory.listFiles();
		if (files != null) {
			for (int i = 0; i < files.length; i++) { // Searching files and making new objects FileFinder
				if (files[i].isDirectory()) {
					pool.execute(new FileFinder(filePattern, files[i], pool));
				} else {
					if (isLikePattern(files[i].getPath())) {
						ThreadPoolTest.out.println(files[i]);
					}
				}
			}
		}
	}

	boolean isLikePattern(String name) { // Check file name for compliance with pattern
		return name.endsWith(filePattern);
	}
}

public class ThreadPoolTest {
	static Scanner in = new Scanner(System.in);
	static PrintWriter out = new PrintWriter(System.out);

	static String getFileType(String pattern) {
		int dotIndex = pattern.indexOf('.');  
		if (dotIndex == -1) {
			return null;
		} else {
			return pattern = pattern.substring(dotIndex + 1);
		}
	}

	public static void main(String[] args) throws InterruptedException {
		out.println("Enter path to find: ");
		out.flush();
		String path = in.nextLine();
		out.println("Enter file pattern to find: ");
		out.flush();
		String pattern = in.nextLine();
		pattern = getFileType(pattern); // Converting string like *.txt to string like txt
		if (pattern == null) { // If pattern was wrong concludes follow Up
			out.println("Wrong pattern");
			out.flush();
			return;
		}
		ThreadPool pool = new ThreadPool(5); // Max amount of threads
		long start = System.currentTimeMillis();
		out.println("Results: ");
		pool.execute(new FileFinder(pattern, new File(path), pool));
		pool.complete();
		pool.shutdown();
		out.println("Time: " + (System.currentTimeMillis() - start));
		out.flush();
	}
}
