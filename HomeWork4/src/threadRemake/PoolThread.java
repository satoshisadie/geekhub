package threadRemake;

public class PoolThread extends Thread{
	private ThreadPool owner;
	
	public PoolThread(ThreadPool owner) {
		this.owner = owner;
	}
	
	public void run() {
		Runnable task;
		do {
			task = owner.getTask();
			if (task != null) {
				task.run();
				owner.done();
			}
		} while (task != null);
	}
}
