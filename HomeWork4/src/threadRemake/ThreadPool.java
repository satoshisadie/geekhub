package threadRemake;

import java.util.*;

public class ThreadPool {
	private PoolThread[] poolThreads;
	private Queue<Runnable> tasks;
	private Integer[] tasksN;

	private void whileNotDone() {
		synchronized (tasksN) {
			try {
				while (tasksN[0] > 0) {
					tasksN.wait();
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	protected void done() {
		synchronized (tasksN) {
			tasksN[0]--;
			tasksN.notify();
		}
	}

	protected Runnable getTask() {
		synchronized (tasks) {
			try {
				while (tasks.isEmpty()) {
					tasks.wait();
				}
				return tasks.poll();
			} catch (InterruptedException e) {
				return null;
			}
		}
	}

	public ThreadPool(int size) {
		this.poolThreads = new PoolThread[size];
		this.tasks = new LinkedList<Runnable>();
		this.tasksN = new Integer[1];
		this.tasksN[0] = new Integer(0);
		for (int i = 0; i < size; i++) {
			poolThreads[i] = new PoolThread(this);
			poolThreads[i].start();
		}
	}

	public void execute(Runnable task) {
		synchronized (tasks) {
			synchronized (tasksN) {
				tasksN[0]++;
			}
			tasks.add(task);
			tasks.notifyAll();
		}
	}

	public void complete() {
		whileNotDone();
	}

	public void shutdown() {
		for (PoolThread thread : poolThreads) {
			thread.interrupt();
		}
	}
}
