import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

class FileFinder implements Runnable {
	private String filePattern; // Pattern to search
	private File directory; // Start directory
	private ExecutorService pool; // Pool of threads
	static private PrintWriter out = new PrintWriter(System.out);
	static private long bTime = -1; // Start time of searching
	static private AtomicInteger N = new AtomicInteger(0); // Amount of objects was created

	public FileFinder(String filePattern, File directory, ExecutorService pool) {
		N.set(N.incrementAndGet()); // New object was created
		this.pool = pool;
		this.filePattern = filePattern;
		this.directory = directory;
		if (bTime == -1) {
			bTime = System.currentTimeMillis(); // First object started work
		}
	}

	public void run() {
		File[] files = directory.listFiles();
		if (files != null) {
			for (int i = 0; i < files.length; i++) { // Searching files and making new objects FileFinder
				if (files[i].isDirectory()) {
					pool.execute(new FileFinder(filePattern, files[i], pool));
				} else {
					if (isLikePattern(files[i].getPath())) {
						out.println(files[i]);
					}
				}
			}
		}
		N.set(N.decrementAndGet()); // If current object finish its work then decrement N
		if (N.compareAndSet(0, 0)) { // If it was last object then shutdown pool and print end time
			pool.shutdown();
			out.println("Time: " + (System.currentTimeMillis() - bTime));
			out.flush();
		}
	}

	boolean isLikePattern(String name) { // Check file name for compliance with pattern
		return name.endsWith(filePattern);
	}
}

public class ThreadsWithConcurrent {
	static Scanner in = new Scanner(System.in);
	static PrintWriter out = new PrintWriter(System.out);

	static String getFileType(String pattern) {
		int dotIndex = pattern.indexOf('.');  
		if (dotIndex == -1) {
			return null;
		} else {
			return pattern = pattern.substring(dotIndex + 1);
		}
	}

	public static void main(String[] args) throws InterruptedException {
		System.out.println("Enter path to find: ");
		String path = in.nextLine();
		System.out.println("Enter file pattern to find: ");
		String pattern = in.nextLine();
		pattern = getFileType(pattern); // Converting string like *.txt to string like txt
		if (pattern == null) { // If pattern was wrong concludes follow Up
			System.out.println("Wrong pattern");
			return;
		}
		ExecutorService pool = Executors.newFixedThreadPool(4); // Max amount of threads
		System.out.println("Results: ");
		pool.execute(new FileFinder(pattern, new File(path), pool));
	}
}
