import java.io.*;
import java.util.*;

class Finder implements Runnable {
	static private PrintWriter out;
	private int from;
	private int to;
	private String pattern;
	private List<File> fileList;

	public Finder(int from, int to, String pattern, List<File> fileList) {
		if (out == null) {
			out = new PrintWriter(System.out);
		}
		this.from = from;
		this.to = to;
		this.pattern = pattern;
		this.fileList = fileList;
	}

	public void run() {
		for (int i = from; i <= to; i++) {
			if (isLikePattern(fileList.get(i).getPath())) {
				out.println(fileList.get(i).getAbsolutePath());
			}
		}
		out.flush();
	}

	boolean isLikePattern(String name) {
		return name.endsWith(pattern);
	}
}

public class ThreadsWithoutConcurrent {

	static Scanner in = new Scanner(System.in);
	static String pattern;
	static List<File> fileList = new ArrayList<File>();

	static void getContents(File dir) {
		File[] files = dir.listFiles();
		if (files != null) {
			for (File file : files) {
				if (file.isDirectory()) {
					getContents(file);
				} else {
					fileList.add(file);
				}
			}
		}
	}
	
	static String getFileType(String pattern) {
		int dotIndex = pattern.indexOf('.');  
		if (dotIndex == -1) {
			return null;
		} else {
			return pattern = pattern.substring(dotIndex + 1);
		}
	}

	public static void main(String[] args) {
		System.out.println("Enter path:");
		String path = in.nextLine();
		System.out.println("Enter pattern:");
		pattern = in.nextLine();
		pattern = getFileType(pattern);
		if (pattern == null) {
			System.out.println("Wrong pattern");
			return;
		} 
		File parentDir = new File(path);
		long startTime = System.currentTimeMillis();
		getContents(parentDir);
		Thread thread1 = new Thread(new Finder(0, fileList.size() / 2, pattern, fileList));
		Thread thread2 = new Thread(new Finder(fileList.size() / 2 + 1, fileList.size() - 1, pattern, fileList));
		thread1.start();
		try {
			thread1.join();
			thread2.join();
		} catch (InterruptedException e) {
			System.out.println(e.getMessage());
		}
		System.out.println("Time: " + (System.currentTimeMillis() - startTime));
	}

}