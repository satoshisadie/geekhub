import java.io.*;
import java.net.*;

public class SimpleClient {

	public static void main(String[] args) throws UnknownHostException,
			IOException {
		Socket socket = new Socket(InetAddress.getLocalHost(), SimpleServer.serverPort);
		BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		PrintWriter out = new PrintWriter(socket.getOutputStream());
		BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
		while (true) {
			String userCommand = keyboard.readLine();
			if (userCommand.equalsIgnoreCase("Exit")) { //User can break program executing typing Exit
				System.out.println("Client disconnected");
				socket.close();
				return;
			}
			out.write(userCommand + "\n");
			out.flush(); //Execute IO thread
			System.out.println("Message sent");
			System.out.println("Server answer: " + in.readLine()); //Client received answer from server
		}
	}
}