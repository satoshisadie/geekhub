import java.io.*;
import java.net.*;

public class SimpleServer {
	static int serverPort = 1025;

	static public void main(String[] args) throws IOException {
		PrintWriter cons = new PrintWriter(System.out);
		ServerSocket serverSocket = new ServerSocket(serverPort);
		cons.println("Waiting for a client");
		cons.flush();
		Socket socket = serverSocket.accept(); // Server waiting for client
		cons.println("Connected"); // Client connected
		cons.flush();
		BufferedReader in = new BufferedReader(new InputStreamReader(
				socket.getInputStream())); // Input stream of socket
		PrintWriter out = new PrintWriter(socket.getOutputStream()); // Output stream of socket
		try {
			String received;
			while ((received = in.readLine()) != null) {
				cons.println("Message received: " + received);
				cons.println("Message sent: "
						+ received.substring(received.length() - 1));
				cons.flush();
				out.write(received.substring(received.length() - 1) + "\n");
				out.flush();
			}
		} catch (Exception e) {
			cons.println(e.getMessage());
			cons.flush();
		} finally {
			socket.close();
			serverSocket.close();
		}
	}
}
