import java.io.*;
import java.net.*;
import java.util.*;

import net.htmlparser.jericho.*;

public class URLGetter {

	static Scanner in = new Scanner(System.in);

	static List<String> code2xx = new ArrayList<>();
	static List<String> code3xx = new ArrayList<>();
	static List<String> code45xx = new ArrayList<>();

	public static void getAll(String url) {
		URLConnection connection;
		List<Element> elements = null;
		try {
			connection = new URL(url).openConnection();
			Source source = new Source(connection);
			elements = source.getAllElements(HTMLElementName.A);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		for (Element element : elements) {
			String adress = element.getAttributeValue("href");
			if (!adress.startsWith("http")) {
				adress = url + adress;
			}
			try {
				HttpURLConnection HttpConnection = (HttpURLConnection) new URL(adress).openConnection();
				HttpConnection.setRequestMethod("HEAD");
				int code = HttpConnection.getResponseCode();
				if (code / 100 == 2) {
					code2xx.add(adress);
				} else if (code / 100 == 3) {
					code3xx.add(adress);
				} else if (code / 100 == 4 || code / 100 == 5) {
					code45xx.add(adress);
				}
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
		}
	}

	public static void main(String[] args) {
		System.out.println("Enter url:");
		String url = in.nextLine();
		getAll(url);
		System.out.println("Code 2xx:");
		for (String adress : code2xx) {
			System.out.println(adress);
		}
		System.out.println("Code 3xx:");
		for (String adress : code3xx) {
			System.out.println(adress);
		}
		System.out.println("Code 4xx, 5xx:");
		for (String adress : code45xx) {
			System.out.println(adress);
		}
	}
}
