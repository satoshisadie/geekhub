

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Action
 */
public class Action extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Action() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		out.write(
			"<body>" +
				"<form action=\"./Action\" method=\"get\">" +
					"<fieldset>" +
						"<table border=\"1\" cellpadding=\"5\" cellspacing=\"2\">" +
							"<tr>" +
								"<td>Message:</td>" +
								"<td><input type=\"text\" name=\"message\" size=30></td>" +
							"</tr>" +
							"<tr>" +
								"<td>Action</td>" +
								"<td>" + 
									"<input type=\"radio\" name=\"action\" value=\"add\" checked=\"checked\" /> add" +
									"<input type=\"radio\" name=\"action\" value=\"update\" /> update" +
									"<input type=\"radio\" name=\"action\" value=\"remove\" /> remove" +
								"<td>" +
							"</tr>" +
							"<tr>" +
								"<td>Name:</td>" +
								"<td><input type=\"text\" name=\"name\" size=30></td>" +
							"</tr>" +
							"<tr>" +
								"<td>Value:</td>" +
								"<td><input type=\"text\" name=\"value\" size=30></td>" +
							"</tr>" +
							"<tr>" +
								"<td>Scope</td>" +
								"<td>" +
									"<input type=\"radio\" name=\"scope\" value=\"session\" checked=\"checked\" /> session" +
									"<input type=\"radio\" name=\"scope\" value=\"app\" /> app" +
								"</td>" +
							"</tr>" +
						"</table>" +
						"<br>" +
						"<input type=\"submit\" value=\"submit\">" +
					"</fieldset>" +
				"</form>" +
			"<body>"
		);
		
		String message = request.getParameter("message");
		String action = request.getParameter("action");
		String name = request.getParameter("name");
		String value = request.getParameter("value");
		String scope = request.getParameter("scope");
		
		HttpSession session = request.getSession();
		ServletContext servletContext = request.getServletContext();
		if ("session".equalsIgnoreCase(scope)) {
			if ("add".equalsIgnoreCase(action) || "update".equalsIgnoreCase(action)) {
				session.setAttribute(name, value);
			} else if ("remove".equalsIgnoreCase(action)) {
				session.removeAttribute(name);
			}
		} else {
			if ("add".equalsIgnoreCase(action) || "update".equalsIgnoreCase(action)) {
				servletContext.setAttribute(name, value);
			} else if ("remove".equalsIgnoreCase(action)) {
				servletContext.removeAttribute(name);
			}
		}
		
		out.write("<h3>Message: " + message + "</h3>");
		Enumeration<String> enumeration = session.getAttributeNames();		
		out.write("<table border=\"1\">");
		out.write("<tr><th colspan=\"2\">Session</th></tr>");
		out.write("<tr><th align=\"left\">Name</th><th align=\"left\">Value</th></tr>");
		while(enumeration.hasMoreElements()) {
			String key = enumeration.nextElement();
			String val = (String) session.getAttribute(key);
			out.write("<tr><td width=\"100\">" + key + "</td><td width=\"100\">" + val + "</td></tr>" );
		}
		out.write("</table><br>");
		
		enumeration = servletContext.getAttributeNames();
		out.write("<table border=\"1\">");
		out.write("<tr><th colspan=\"2\">ServletContext</th></tr>");
		out.write("<tr><th align=\"left\">Name</th><th align=\"left\">Value</th></tr>");
		while(enumeration.hasMoreElements()) {
			String key = enumeration.nextElement();
			out.write("<tr><td width=\"100\">" + key + "</td><td width=\"100\">" + servletContext.getAttribute(key) + "</td></tr>" );
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
